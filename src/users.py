class User:
    def __init__(self, email):
        self.email = email

    def say_hello(self):
        print(f'Hey! My email is {self.email}')


if __name__ == '__main__':
    kate = User('kate@example.com')
    john = User('john@example.com')

    print(kate.email)
    print(john.email)
    kate.say_hello()
    john.say_hello()
