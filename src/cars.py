from datetime import datetime


class Car:
    def __init__(self, brand, colour, production_year):
        self.brand = brand
        self.colour = colour
        self.production_year = production_year
        self.mileage = 0

    def drive(self, distance):
        self.mileage += distance

    def get_age(self):
        return datetime.now().year - self.production_year

    def repaint(self, new_colour):
        self.colour = new_colour


if __name__ == '__main__':
    car1 = Car('Toyota', 'black', 2001)
    car2 = Car('Honda', 'black', 2021)
    car3 = Car('Audi', 'black', 1997)

    print(car1.mileage)
